from datetime import datetime
from pathlib import Path
from string import Template

import click as click

from aoc_2021 import paths


def template_solution() -> str:
    with (paths.solutions() / "template.py").open("r") as fp:
        return fp.read()


def solution_path_for_day(day_number: int) -> Path:
    return paths.solutions() / f"day_{day_number}.py"


def input_path_for_day(day_number: int):
    return paths.inputs() / f"day_{day_number}.txt"


def example_input_path_for_day(day_number: int):
    return paths.inputs() / f"day_{day_number}.example.txt"


@click.group()
def cli():
    pass


@cli.command()
@click.option("--day_number", type=int, required=False)
def make_day(day_number):
    if day_number is None:
        today = datetime.now().date()
        day_number = today.day

    solution_path = solution_path_for_day(day_number)
    input_path = input_path_for_day(day_number)
    example_input_path = example_input_path_for_day(day_number)

    if any((solution_path.exists(), input_path.exists(), example_input_path.exists())):
        print(f"Data already exists for day {day_number}")
        return

    template = Template(template_solution())
    solution_string = template.safe_substitute({
        "DAY_NUMBER": str(day_number)
    })

    with solution_path.open("w+") as fp:
        fp.write(solution_string)

    input_path.touch()
    example_input_path.touch()


if __name__ == '__main__':
    cli()
