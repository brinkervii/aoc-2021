from pathlib import Path

HERE = Path(__file__).resolve().parent


def inputs() -> Path:
    return HERE / "inputs"


def solutions() -> Path:
    return HERE / "solutions"
