from time import perf_counter


class Stopwatch:
    _start_time: float
    _stop_time: float
    _name: str

    def __init__(self, name: str):
        self._start()
        self._name = name

    def _start(self):
        self._start_time = perf_counter()

    def _stop(self):
        self._stop_time = perf_counter()

    @property
    def delta(self) -> float:
        return self._stop_time - self._start_time

    def __enter__(self):
        self._start()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._stop()
        print(self)

    def __str__(self):
        return f"⏱ <{type(self).__name__}> 🏷 {self._name} >> ⌛ {self.delta}"
