from typing import Tuple, List

from aoc_2021 import paths
from aoc_2021.stopwatch import Stopwatch

Report = List[int]
BiSlidingWindow = Tuple[int, int]
Delta = Tuple[int, int]


def _read_report() -> Report:
    input_file = paths.inputs() / "day_1.txt"

    with input_file.open("r") as fp:
        return list(map(int, filter(None, fp.readlines())))


def _measure_report(report) -> int:
    def measure_delta(t: BiSlidingWindow) -> Delta:
        cur, prev = t

        return cur, 1 if cur > prev else -1

    return sum(
        map(
            lambda t: t[1],
            filter(
                lambda t: t[1] > 0,
                map(measure_delta, zip(report[1:], report))
            )
        )
    )


def part_1():
    report = _read_report()
    depth_increase = _measure_report(report)

    print(f"🌟 Part 1 :: Depth increase = {depth_increase}")


def part_2():
    report = _read_report()

    transformed_report = list(
        map(
            sum,
            zip(
                report,
                report[1:],
                report[2:]
            )
        )
    )

    depth_increase = _measure_report(transformed_report)

    print(f"🌟 Part 2 :: Depth increase = {depth_increase}")


if __name__ == '__main__':
    with Stopwatch("AOC Day 1, Part 1"):
        part_1()

    with Stopwatch("AOC Day 1, Part 2"):
        part_2()
