import re
from dataclasses import dataclass
from typing import Tuple, List, Callable, Dict

from aoc_2021 import paths
from aoc_2021.stopwatch import Stopwatch

INSTRUCTION_PTN = re.compile(r"^(\w+)\s+([-\d]+)")

Instruction = Tuple[str, int]
Program = List[Instruction]
Command = Callable[[int], None]
CommandSet = Dict[str, Command]


@dataclass
class Vector:
    x: int = 0  # Horizontal
    y: int = 0  # Depth
    z: int = 0  # Aim


def _read_input() -> Program:
    input_file = paths.inputs() / "day_2.txt"

    def map_line(line: str) -> Instruction:
        match = INSTRUCTION_PTN.search(line.strip())
        assert match, f"Invalid line: {line}"

        return match.group(1), int(match.group(2))

    with input_file.open("r") as fp:
        return list(map(map_line, filter(None, fp.readlines())))


def _create_command_set(*commands) -> CommandSet:
    return dict(map(lambda f: (f.__name__, f), commands))


def _command_set_a(position: Vector) -> CommandSet:
    def forward(amount):
        position.x += amount

    def down(amount):
        position.y += amount

    def up(amount):
        position.y -= amount

    return _create_command_set(forward, down, up)


def _command_set_b(position: Vector) -> CommandSet:
    def forward(amount):
        position.x += amount
        position.y += (position.z * amount)

    def down(amount):
        position.z += amount

    def up(amount):
        position.z -= amount

    return _create_command_set(forward, down, up)


def _process_commands(program: Program, aim: bool) -> int:
    position = Vector()

    commands = _command_set_b(position) if aim else _command_set_a(position)

    for command, amount in program:
        commands[command](amount)

    return position.x * position.y


def part_1():
    result = _process_commands(_read_input(), aim=False)
    print(f"🌟 Part 1 :: Result = {result}")


def part_2():
    result = _process_commands(_read_input(), aim=True)
    print(f"🌟 Part 2 :: Result = {result}")


if __name__ == '__main__':
    with Stopwatch("AOC Day 2, Part 1"):
        part_1()

    with Stopwatch("AOC Day 2, Part 2"):
        part_2()
