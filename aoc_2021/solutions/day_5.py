import re
import sys
from dataclasses import dataclass
from typing import List, Tuple, Dict, Iterable, Optional

from aoc_2021 import paths
from aoc_2021.stopwatch import Stopwatch

LINE_PTN = re.compile(r"^([-\d]+),([-\d]+)\s+->\s+([-\d]+),([-\d]+)")


@dataclass
class Line:
    x1: int
    y1: int
    x2: int
    y2: int

    @property
    def is_horizontal(self):
        return self.y1 == self.y2

    @property
    def is_vertical(self):
        return self.x1 == self.x2

    @property
    def is_cardinal(self):
        return self.is_horizontal or self.is_vertical

    def __repr__(self):
        return str(self)

    def __str__(self):
        return f"({self.x1}, {self.y1}) -> ({self.x2}, {self.y2})"


@dataclass
class Cell:
    height: int = 0


CellIndex = Tuple[int, int]


class View:
    _sparse_grid: Dict[CellIndex, Cell]

    _min: Tuple[int, int]
    _max: Tuple[int, int]

    def __init__(self):
        self._sparse_grid = dict()
        self._min = (sys.maxsize, sys.maxsize)
        self._max = (-sys.maxsize, -sys.maxsize)

    def _cell_at(self, key: CellIndex, create: bool = True) -> Optional[Cell]:
        if cell := self._sparse_grid.get(key):
            return cell

        if create:
            cell = Cell()
            self._sparse_grid[key] = cell

            return cell

        return None

    def add_line(self, line: Line):
        y = line.y1
        x = line.x1

        x_direction = 1 if line.x2 > line.x1 else -1
        y_direction = 1 if line.y2 > line.y1 else -1

        while True:
            cell = self._cell_at((x, y))
            cell.height += 1

            self._min = (
                min(self._min[0], x),
                min(self._min[1], y)
            )

            self._max = (
                max(self._max[0], x),
                max(self._max[1], y)
            )

            if x == line.x2 and y == line.y2:
                break

            if x != line.x2:
                x += x_direction

            if y != line.y2:
                y += y_direction

    def add_lines(self, lines: Iterable[Line]):
        for line in lines:
            self.add_line(line)

    @property
    def cells_with_overlap(self) -> List[Cell]:
        return list(filter(lambda cell: cell.height >= 2, self._sparse_grid.values()))

    def __str__(self):
        view = []

        for y in range(self._min[1], self._max[1] + 1):
            line = []
            for x in range(self._min[0], self._max[0] + 1):
                if cell := self._cell_at((x, y), create=False):
                    line.append(str(cell.height))

                else:
                    line.append(".")

            view.append("".join(line))

        return "\n".join(view)


SensorLog = List[Line]


def _read_sensor_log() -> SensorLog:
    input_file = paths.inputs() / "day_5.txt"

    def map_line(line: str):
        match = LINE_PTN.search(line.strip())
        assert match, f"Invalid line: {line}"

        return Line(*tuple(map(int, match.groups())))

    with input_file.open("r") as fp:
        return list(map(map_line, filter(None, fp.readlines())))


def _inspect_sensor_log(sensor_log: SensorLog, cardinals_only: bool) -> int:
    if cardinals_only:
        subjects = list(filter(lambda ln: ln.is_cardinal, sensor_log))

    else:
        subjects = sensor_log

    view = View()
    view.add_lines(subjects)
    overlap = view.cells_with_overlap

    return len(overlap)


def part_1():
    solution = _inspect_sensor_log(_read_sensor_log(), True)
    print(f"🌟 Part 1 :: #Points with overlap = {solution}")


def part_2():
    solution = _inspect_sensor_log(_read_sensor_log(), False)
    print(f"🌟 Part 2 :: #Points with overlap = {solution}")


if __name__ == '__main__':
    with Stopwatch("AOC Day 5, Part 1"):
        part_1()

    with Stopwatch("AOC Day 5, Part 2"):
        part_2()
