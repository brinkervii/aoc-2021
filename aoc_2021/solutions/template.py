from aoc_2021 import paths
from aoc_2021.stopwatch import Stopwatch


def _read_input():
    input_file = paths.inputs() / "day_$DAY_NUMBER.example.txt"

    with input_file.open("r") as fp:
        return fp.read()


def part_1():
    solution = 1
    print(f"🌟 Part 1 :: Solution = {solution}")


def part_2():
    solution = 1
    print(f"🌟 Part 2 :: Solution = {solution}")


if __name__ == '__main__':
    with Stopwatch("AOC Day $DAY_NUMBER, Part 1"):
        part_1()

    with Stopwatch("AOC Day $DAY_NUMBER, Part 2"):
        part_2()
