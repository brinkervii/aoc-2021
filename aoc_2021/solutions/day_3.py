from copy import deepcopy
from typing import Tuple, List, Dict

from aoc_2021 import paths
from aoc_2021.stopwatch import Stopwatch


def _read_input():
    input_file = paths.inputs() / "day_3.txt"

    def map_line(line: str):
        return tuple(map(int, line.strip()))

    with input_file.open("r") as fp:
        return list(map(map_line, filter(None, fp.readlines())))


def _get_col(report: List[Tuple[int, ...]], index: int):
    return tuple(
        map(
            lambda row: row[index],
            report
        )
    )


def _width(report: List[Tuple[int, ...]]) -> int:
    return max(
        map(
            lambda row: len(row),
            report
        )
    )


def _bit_counts(col: Tuple[int, ...]) -> Tuple[str, str, bool]:
    bean_counter: Dict[str, int] = dict()

    for v in col:
        k = str(v)

        x = bean_counter.setdefault(k, 0)
        x += 1
        bean_counter[k] = x

    bean_counter: List[Tuple[str, int]] = list(zip(bean_counter.keys(), bean_counter.values()))

    sorted_counter = sorted(bean_counter, key=lambda t: t[1])
    least_common = sorted_counter[0]
    most_common = sorted_counter[-1]
    equally_common = most_common[1] == least_common[1]

    return most_common[0], least_common[0], equally_common


def _power_consumption(diagnostic_report):
    gamma_components = []
    epsilon_components = []

    for position in range(_width(diagnostic_report)):
        col = _get_col(diagnostic_report, position)
        most_common_bit, least_common_bit, _ = _bit_counts(col)

        gamma_components.append(most_common_bit)
        epsilon_components.append(least_common_bit)

    gamma = int("".join(gamma_components), 2)
    epsilon = int("".join(epsilon_components), 2)

    return gamma * epsilon


def _get_oxygen_rating(diagnostic_report):
    oxygen_report = deepcopy(diagnostic_report)
    for position in range(_width(diagnostic_report)):
        mc, _, eq = _bit_counts(_get_col(oxygen_report, position))

        keep = 1 if eq else int(mc)

        oxygen_report = list(filter(lambda row: row[position] == keep, oxygen_report))

        if len(oxygen_report) == 1:
            break

    return int("".join(list(map(str, oxygen_report[0]))), 2)


def _get_co2_rating(diagnostic_report):
    co2_report = deepcopy(diagnostic_report)

    for position in range(_width(diagnostic_report)):
        _, lc, eq = _bit_counts(_get_col(co2_report, position))

        keep = 0 if eq else int(lc)

        co2_report = list(filter(lambda row: row[position] == keep, co2_report))

        if len(co2_report) == 1:
            break

    return int("".join(list(map(str, co2_report[0]))), 2)


def _life_support_rating(diagnostic_report):
    oxygen_rating = _get_oxygen_rating(diagnostic_report)
    co2_rating = _get_co2_rating(diagnostic_report)

    return oxygen_rating * co2_rating


def part_1():
    diagnostic_report = _read_input()
    power_consumption = _power_consumption(diagnostic_report)

    print(f"🌟 Part 1 :: Power Consumption = {power_consumption}")


def part_2():
    diagnostic_report = _read_input()
    lsr = _life_support_rating(diagnostic_report)
    print(f"🌟 Part 2 :: Life Support Rating = {lsr}")


if __name__ == '__main__':
    with Stopwatch("AOC Day 3, Part 1"):
        part_1()

    with Stopwatch("AOC Day 3, Part 2"):
        part_2()
