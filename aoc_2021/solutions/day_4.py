import re
from dataclasses import dataclass
from typing import List

from aoc_2021 import paths
from aoc_2021.stopwatch import Stopwatch


@dataclass(init=False)
class BingoBoard:
    data: List[List[int]]
    marks: List[List[bool]]

    def __init__(self, data: List[List[int]]):
        self.data = data

        self._width = max(map(lambda row: len(row), data))
        self._height = len(data)

        self.marks = [[False for _x in range(self._width)] for _x in range(self._height)]

    def mark_column(self, col_index: int):
        return tuple(map(lambda row: row[col_index], self.marks))

    def mark_number(self, n: int):
        for y in range(self._height):
            for x in range(self._width):
                v = self.data[x][y]
                if v == n:
                    self.marks[x][y] = True
                    return

    @property
    def win(self) -> bool:
        any_row = any(map(all, self.marks))
        if any_row:
            return True

        any_col = any([all(self.mark_column(i)) for i in range(self._width)])

        if any_col:
            return True

        return False

    @property
    def unmarked_data(self):
        data = []

        for row_index in range(self._height):
            row = []
            data_row = self.data[row_index]
            mark_row = self.marks[row_index]

            for col_index in range(self._width):
                row.append(None if mark_row[col_index] else data_row[col_index])

            data.append(row)

        return data


@dataclass
class PuzzleInput:
    instructions: List[int]
    boards: List[BingoBoard]

    @property
    def losing_boards(self):
        return list(filter(lambda board: not board.win, self.boards))


def _read_input():
    input_file = paths.inputs() / "day_4.txt"

    boards = []
    bb_lines = []

    def next_board():
        if len(bb_lines) <= 0:
            return

        data = list(
            map(
                lambda s: list(map(int, map(str.strip, filter(None, re.split(r"\s+", s.strip()))))),
                bb_lines
            )
        )

        boards.append(BingoBoard(data))

        bb_lines.clear()

    with input_file.open("r") as fp:
        instructions = fp.readline()

        while line := fp.readline():
            if line.strip():
                bb_lines.append(line)

            else:
                next_board()

        next_board()

        return PuzzleInput(
            list(map(int, map(str.strip, filter(None, instructions.split(","))))),
            boards
        )


def _data_generator(data):
    for row in data:
        for col in row:
            if col is not None:
                yield col


def _run_game(data: PuzzleInput):
    for i, draw in enumerate(data.instructions):
        for board in data.boards:
            board.mark_number(draw)

            if board.win:
                return i, draw, board

    assert "Failure"


def part_1():
    data = _read_input()
    i, draw, winning_board = _run_game(data)

    x = winning_board.unmarked_data
    y = list(_data_generator(x))
    v = sum(y) * draw

    print(f"🌟 Part 1 :: Bingo value = {v}")


def _run_losing_game(data: PuzzleInput):
    for i, draw in enumerate(data.instructions):
        losing_boards = data.losing_boards

        for board in data.boards:
            board.mark_number(draw)

        if len(losing_boards) == 1:
            loser = losing_boards[0]

            if loser.win:
                return i, draw, loser


def part_2():
    data = _read_input()
    i, draw, winning_board = _run_losing_game(data)

    x = winning_board.unmarked_data
    y = list(_data_generator(x))
    u = sum(y)
    v = u * draw

    print(f"🌟 Part 2 :: Bingo value of the loser = {v}")


if __name__ == '__main__':
    with Stopwatch("AOC Day 4, Part 1"):
        part_1()

    with Stopwatch("AOC Day 4, Part 2"):
        part_2()
